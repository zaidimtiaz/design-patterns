from itertools import combinations


def largest_possible_loss(pricesLst):
    # get all possible combinations of price
    comb = combinations(pricesLst, 2)
    loss = []
    for ele1, ele2 in list(comb):
        # check if index1 < index2 in pricesLst
        if pricesLst.index(ele1) < pricesLst.index(ele2):
            # pricesLst[index2] - pricesLst[index1]
            loss.append(ele2 - ele1)

    return max(loss)


def test_largest_possible_loss():
    pricesLst = [8, 10, 7, 5, 7, 15]

    assert largest_possible_loss(pricesLst) == 10
