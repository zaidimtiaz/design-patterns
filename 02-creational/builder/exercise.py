# Add functionaity to incorporate class definitions in the current implementation
# new output:
import datetime

# class Person:
#    def __init__(self):
#        self.name = ""
#        self.age = 0


#   def get_dob():
#        print("to something")

 #   def somemthod():
 #       print("to something")





class Field:
    def __init__(self, name, value):
        self.value = value
        self.name = name

    def __str__(self):
        return 'self.%s = %s' % (self.name, self.value)


class Class:
    def __init__(self, name):
        self.name = name
        self.fields = []
        self.definitions = []

    def __str__(self):
        lines = ['class %s:' % self.name]
        if not self.fields:
            lines.append('  pass')
        else:
            lines.append('  def __init__(self):')
            for f in self.fields:
                lines.append('    %s' % f)
        return '\n'.join(lines)


class CodeBuilder:
    def __init__(self, root_name):
        self.__class = Class(root_name)

    def add_field(self, type, name):
        self.__class.fields.append(Field(type, name))
        return self

    def __str__(self):
        return self.__class.__str__()



cb = CodeBuilder('Person').add_field('name','"Zaid"') \
    .add_field('age','35')\
    .add_field('address','"Finland,Espoo"')

print(cb)