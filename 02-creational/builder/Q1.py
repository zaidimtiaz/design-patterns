from unittest import TestCase


def increment_dictionary_values(d, i):
    # list and dic are immutable therefore we use for dict to create a copy
    dic = dict(d)
    for k, v in dic.items():
        dic[k] = v + i
    return dic


class TestIncrementDictionaryValues(TestCase):
    def test_increment_dictionary_values(self):
        d = {'a': 1}
        dd = increment_dictionary_values(d, 1)
        ddd = increment_dictionary_values(d, -1)
        self.assertEqual(dd['a'], 2)
        self.assertEqual(ddd['a'], 0)
