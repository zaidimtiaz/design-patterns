# We have a SQL table with 4 columns (id, url, date, rating). In our development we face many
# cases where we need to pull data from the database. Most often the developer chooses to
# embed the SQL command directly into the python code but over time we found self error
# prone and difficult to test.
# We want to design a set of classes that allows us to build a query which can filter self table
# across any combination of these possibilities:
# ● id: >, <, =, IN, NOTIN
# ● url:=
# ● date: >, <, =
# ● rating: >, <, =
# e.g. we may want all entries with: (2 < rating < 9) and (id in list) and (date > 1 Jan 2016).
# The goal is to have a set of classes which enable easy testing wherever they are used (i.e.
# the database does not have to be overly mocked every time). We want users of these
# classes to be able to add filters without having to add to or rewrite tests.



# Solution
# the solution follows a builder design pattern to construct queries to avoid messy string formatting
# The solution only returns the actual SQL which can be used to fetch data

from abc import ABC, abstractmethod


class Query:
    def __init__(self):
        self.sql = None

    def __str__(self):
        return str(self.query)


class QueryBuilder(ABC):

    @abstractmethod
    def table(self, table):
        pass

    @abstractmethod
    def where(self, where):
        pass

    @abstractmethod
    def select(self, select):
        pass

    @abstractmethod
    def get_sql(self):
        pass

    @abstractmethod
    def all(self):
        pass

class SQLBuilder(QueryBuilder):
    def __init__(self):
        self.query = Query()
        self._from = None
        self._where = None
        self._select = "*"

    def __str__(self):
        return self.get_sql()

    def table(self, table):
        self._from = table
        return self

    def where(self, where):
        self._where = where
        return self

    def select(self, select):
        self._select = select
        return self

    def get_sql(self):
        if self._from:
            self.query.sql = "Select {0} from {1}".format(self._select, self._from)
            if self._where:
                self.query.sql = "Select {0} from {1} where {2}".format(self._select, self._from, self._where)
        return self.query.sql


query = SQLBuilder()
ids = ('1', '2', '3', '4')
print(query.table("rating").select("id,rating,date").where("rating > 9 and id IN {0}".format(ids)))
