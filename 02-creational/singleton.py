class Singleton:
    _instance = None

    def __init__(self, name):
        print(f"- Instance initialization: name={name}")
        self.name = 'Alex'

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            print("- Creating new instance")
            cls._instance = object.__new__(cls)
        print("- Returning existing instance")
        return cls._instance


class NotSingleton:

    def __init__(self, name):
        print(f"- Instance initialization: name={name}")
        self.name = name


singleton = Singleton("hello")
singleton2 = Singleton("john")
not_singleton = NotSingleton("hello")

if singleton == singleton2:
    print("True")
else:
    print("False")

if singleton == not_singleton:
    print("True")
else:
    print("False")

print(singleton.name)
print(singleton2.name)
